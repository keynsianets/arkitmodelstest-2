//
//  ViewController.swift
//  ARKitModelsTest
//
//  Created by Денис Марков on 2/13/19.
//  Copyright © 2019 Денис Марков. All rights reserved.
//

import UIKit
import SceneKit
import GLTFSceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {
    
    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var capittoneButton: UIButton! {
        didSet {
            capittoneButton.addTarget(self, action: #selector(capittoneButtonDidTap), for: .touchUpInside)
        }
    }
    @IBOutlet weak var swivelButton: UIButton! {
        didSet {
            swivelButton.addTarget(self, action: #selector(swivelButtonDidTap), for: .touchUpInside)
        }
    }
    
    var nodeModel: SCNNode?
    
    var capittone: SCNNode?
    
    var swivel: SCNNode?
    
    var currentNode: SCNNode?
    
    //Store The Rotation Of The CurrentNode
    var currentAngleY: Float = 0.0
    
    //Not Really Necessary But Can Use If You Like
    var isRotating = false
    
    @IBOutlet weak var noPlane: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        //sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
        // sceneView.antialiasingMode = .multisampling4X
        
        // Create a new scene
        let scene = SCNScene()
        
        // Set the scene to the view
        sceneView.scene = scene
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
        self.sceneView.addGestureRecognizer(tapGestureRecognizer)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(moveNode(_:)))
        self.view.addGestureRecognizer(panGesture)
        
        let rotateGesture = UIRotationGestureRecognizer(target: self, action: #selector(rotateNode(_:)))
        self.view.addGestureRecognizer(rotateGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Pause the view's session
        sceneView.session.pause()
    }
    
    @objc func capittoneButtonDidTap() {
        nodeModel = capittone
    }
    
    @objc func swivelButtonDidTap() {
        nodeModel = swivel
    }
    
    @objc func tapped(recognizer: UITapGestureRecognizer) {
        debugPrint("tap")
        guard let sceneView = recognizer.view as? ARSCNView else {
            return
        }
        debugPrint("view")
        let touch = recognizer.location(in: sceneView)
        
        let hitTestResults = sceneView.hitTest(touch, types: .existingPlane)
        
        if let hitTest = hitTestResults.first {
            addFoodModelTo (position: SCNVector3(hitTest.worldTransform.columns.3.x,hitTest.worldTransform.columns.3.y,hitTest.worldTransform.columns.3.z))
            //            guard let chairNode = nodeModel else {
            //                debugPrint("no model")
            //                return
            //            }
            //            debugPrint("position")
            //            chairNode.position = SCNVector3(hitTest.worldTransform.columns.3.x,hitTest.worldTransform.columns.3.y,hitTest.worldTransform.columns.3.z)
            //
            //            self.sceneView.scene.rootNode.addChildNode(chairNode)
            //            currentNode = chairNode
        }
        
        
    }
    
    func addFoodModelTo(position: SCNVector3) {
        guard let fruitCakeScene = SCNScene(named: "art.scnassets/Shtora/Shtora_1_M3.dae") else {
            fatalError("Unable to find model")
        }
        let baseNode = fruitCakeScene.rootNode
        baseNode.position = position
        baseNode.scale = SCNVector3Make(0.2, 0.2, 0.2)
        let cakeNode = baseNode.childNode(withName: "shtora", recursively: true)
        let cakeMaterial = SCNMaterial()
        // The lightingModel of the material has to be set to .physicallyBased to take advantage of the environment lighting
        cakeMaterial.lightingModel = .physicallyBased
        guard let image = UIImage(named: "art.scnassets/Shtora/yellow.jpg") else { return }
        cakeMaterial.diffuse.contents = image
        cakeMaterial.diffuse.wrapS = .repeat
        cakeMaterial.diffuse.wrapT = .repeat
        cakeMaterial.diffuse.mipFilter = .linear
        cakeMaterial.diffuse.contentsTransform = SCNMatrix4MakeScale(Float(10), Float(10), 1)
        //            cakeMaterial.normal.contents = UIImage(named: "art.scnassets/Shtora/Navona_stoneQ_C.png")
        cakeMaterial.normal.intensity = 0.5
        cakeNode?.geometry?.firstMaterial = cakeMaterial
        
        if sceneView.scene.rootNode.childNodes.count > 0 {
            sceneView.scene.rootNode.replaceChildNode(sceneView.scene.rootNode.childNodes[0], with: baseNode)
        } else {
            sceneView.scene.rootNode.addChildNode(baseNode)
        }
        
        addPlaneTo(node: baseNode)
    }
    
    func addPlaneTo(node:SCNNode) {
        // Create a plane that only receives shadows
        let plane = SCNPlane(width: 200, height: 200)
        plane.firstMaterial?.colorBufferWriteMask = .init(rawValue: 0)
        let planeNode = SCNNode(geometry: plane)
        planeNode.rotation = SCNVector4Make(1, 0, 0, -Float.pi / 2)
        node.addChildNode(planeNode)
    }
    
    /// Rotates An Object On It's YAxis
    ///
    /// - Parameter gesture: UIPanGestureRecognizer
    @objc func moveNode(_ gesture: UIPanGestureRecognizer) {
        debugPrint("move")
        if !isRotating{
            
            //1. Get The Current Touch Point
            let currentTouchPoint = gesture.location(in: self.sceneView)
            
            //2. Get The Next Feature Point Etc
            guard let hitTest = self.sceneView.hitTest(currentTouchPoint, types: .existingPlane).first else { return }
            
            //3. Convert To World Coordinates
            let worldTransform = hitTest.worldTransform
            
            //4. Set The New Position
            let newPosition = SCNVector3(worldTransform.columns.3.x, worldTransform.columns.3.y, worldTransform.columns.3.z)
            
            //5. Apply To The Node
            currentNode?.simdPosition = float3(newPosition.x, newPosition.y, newPosition.z)
            
        }
    }
    
    /// Rotates An SCNNode Around It's YAxis
    ///
    /// - Parameter gesture: UIRotationGestureRecognizer
    @objc func rotateNode(_ gesture: UIRotationGestureRecognizer){
        debugPrint("rotate", isRotating)
        //1. Get The Current Rotation From The Gesture
        let rotation = Float(gesture.rotation)
        
        //2. If The Gesture State Has Changed Set The Nodes EulerAngles.y
        if gesture.state == .changed{
            isRotating = true
            
            currentNode?.eulerAngles.y = currentAngleY + rotation
            
            
        }
        
        //3. If The Gesture Has Ended Store The Last Angle Of The Cube
        if(gesture.state == .ended) {
            currentAngleY = currentNode?.eulerAngles.y ?? 0
            isRotating = false
        }
    }
    
    func doHitTestOnExistingPlanes() -> SCNVector3? {
        // hit-test of view's center with existing-planes
        let results = sceneView.hitTest(view.center,
                                        types: .existingPlane)
        // check if result is available
        if let result = results.first {
            // get vector from transform
            let hitPos = self.positionFromTransform(result.worldTransform)
            return hitPos
        }
        return nil
    }
    
    // get position 'vector' from 'transform'
    func positionFromTransform(_ transform: matrix_float4x4) -> SCNVector3 {
        return SCNVector3Make(transform.columns.3.x,
                              transform.columns.3.y,
                              transform.columns.3.z)
    }
    
    // MARK: - ARSCNViewDelegate
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        /*if !anchor.isKind(of: ARPlaneAnchor.self) {
         DispatchQueue.main.async {
         if let modelClone = self.nodeModel?.clone() {
         modelClone.position = SCNVector3Zero
         
         // Add model as a child of the node
         node.addChildNode(modelClone)
         self.currentNode = modelClone
         } else {
         
         }
         }
         }*/
    }
    
    func renderer(_ renderer: SCNSceneRenderer,
                  updateAtTime time: TimeInterval) {
        DispatchQueue.main.async {
            
            // get current hit position
            // and check if start-node is available
            guard let currentPosition = self.doHitTestOnExistingPlanes() else {
                self.noPlane.isHidden = false
                //self.notReadyLabel.text = "Move your device to find a plane"
                return
            }
            self.noPlane.isHidden = true
        }
    }
    
    /*
     // Override to create and configure nodes for anchors added to the view's session.
     func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
     let node = SCNNode()
     
     return node
     }
     */
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
